import React from 'react'


const UserContext = React.createContext()

export const UserProvider = UserContext.Provider

export default UserContext



//useState
/*
	initial value of empty/0/false/true
	setter - to set the new value of getter
*/


//useEffect
/*
	if there are changes in the component, or display of the component. Then it will run the function

	*dependency array is needed [value1, value2]
*/


//useContext
/*
	UserProvider(App.js) - source of data that will make accessable for all path/files
	
	useContext - global value that can be used to files
*/