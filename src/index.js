import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import AppNavbar from './AppNavbar';

// import {Navbar,Nav,Container} from 'react-bootstrap';

//Import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "Tristan Parcon"
