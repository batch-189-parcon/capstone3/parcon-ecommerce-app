import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){

	const { coursesProp, fetchData } = props;

	const [productArr, setProductArr] = useState([])
	const [productId, setProductId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [image, setImage] = useState("")
	const [price, setPrice] = useState(0)
	const [showAdd, setShowAdd] = useState(false)
	const [showEdit, setShowEdit] = useState(false)

	const token = localStorage.getItem("token")

	
	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)

	const openEdit = (productId) => {
		fetch(`https://vast-shelf-02215.herokuapp.com/products/${productId}`)
		.then(response => response.json())
		.then(data => {
			// console.log(data)
			setProductId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setImage(data.image)
		})
		//populate the edit modal's input fields with the proper information
		setShowEdit(true)
	}

	const closeEdit = () => {
		setName("")
		setDescription("")
		setPrice(0)
		setShowEdit(false)
	}

	const addProduct = (event) => {
		event.preventDefault()

		fetch(`https://vast-shelf-02215.herokuapp.com/products/addProduct`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				image: image
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully added"
				})

				fetchData()
				closeAdd()

				setName("")
				setDescription("")
				setPrice(0)
				setImage("")
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})

				fetchData()		
			}
		})
	}

	const editProduct = (event) => {
		event.preventDefault()

		fetch(`https://vast-shelf-02215.herokuapp.com/products/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				image: image
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully updated"
				})

				fetchData()
				closeEdit()

				setName("")
				setDescription("")
				setPrice(0)
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})

				fetchData()		
			}
		})
	}

	const archiveToggle = (productId, isActive) => {
		fetch(`https://vast-shelf-02215.herokuapp.com/${productId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(response => response.json())
		.then(data => {
			// console.log(data)
			if(data){
				let bool

				isActive ? bool = "disabled" : bool = "enabled"

				fetchData()

				Swal.fire({
					title: "Sucess",
					icon: "success",
					text: `Product successfully ${bool}`
				})
			}else{
				fetchData()

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}

		})
	}

	useEffect(() => {
		const products = coursesProp.map(product => {
			return(
				<tr key={product._id}>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>

					<td>
							{product.isActive
								? <span>Available</span>
								: <span>Unavailable</span>
							}
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={() => openEdit(product._id)}>Update</Button>
						{product.isActive
							? <Button variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
							: <Button variant="success" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>
						}
					</td>
				</tr>
			)
		})

		setProductArr(products)

	}, [coursesProp])
	
	return(
		<>
			<div className="text-center my-4">
				<h2>Admin Dashboard</h2>
				<Button variant="primary" onClick={openAdd}>Add New Product</Button>
			</div>

			{/*Product info table*/}
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{productArr}
				</tbody>
			</Table>

			{/*Add Product Modal*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={event => addProduct()}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={event => setName(event.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={event => setDescription(event.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={event => setPrice(event.target.value)}
								type="number"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productImage">
							<Form.Label>Image</Form.Label>
							<Form.Control
								value={image}
								onChange={event => setImage(event.target.value)}
								type="text"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>




			{/*Edit Product Modal*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={event => editProduct(event)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={event => setName(event.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={event => setDescription(event.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={event => setPrice(event.target.value)}
								type="number"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productImage">
							<Form.Label>Image</Form.Label>
							<Form.Control
								value={image}
								onChange={event => setImage(event.target.value)}
								type="string"
								required
							/>
						</Form.Group>

					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
	)
}
