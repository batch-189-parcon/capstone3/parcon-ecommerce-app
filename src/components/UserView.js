import { useState, useEffect } from 'react';
import ProductCard from './ProductCard'

export default function UserView({coursesProp}) {
	const [productArr, setProductArr] = useState([])
	useEffect(() => {
		const products = coursesProp.map(product => {
			if(product.isActive){
				return <ProductCard key={product._id} courseProp={product} />
			}else{
				return null
			}
		})
		setProductArr(products)
	}, [coursesProp])
	return(
		<>
			{productArr}
		</>
	)
}
