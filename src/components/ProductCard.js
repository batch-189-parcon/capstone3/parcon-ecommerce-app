// import { useState, useEffect } from 'react';
import { Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function ProductCard ({courseProp}) {

	// console.log(props)
	// console.log(typeof props)
	// console.log(props.courseProp.name)
	//console.log(courseProp)

	/*
	Use the state hook for this component to be able store its state.
	States are used to keep track of informatuion related to individual components.

	Syntax: 
		const [getter, setter] = useState(initialGettterValue)

	*/
	//Setter to update the value of variable
	// const [count, setCount] = useState(0)
	// const [seats, setSeat] = useState(30)

	// function enroll() {
	// 	// if (seats > 0) {
	// 		setSeat(seats - 1)
	// 		console.log('Seats:' + seats)
	// 		setCount(count + 1)
	// 		console.log('Enrollees:' + count)
	// 	// } else {
	// 	// 	alert("No more seats")
	// 	// }
 // }
 // 	//verify if number of seats to 0. and will alert to no more seats available.	
 // 	useEffect(() => {
 // 		if (seats === 0 ) {
 // 			alert ('No more seats available')
 // 		}
 // 	}, [seats])
	

	const { name, description, price, _id } = courseProp;
	console.log(courseProp)

	return (
					<Card>
      					<Card.Body>
        					<Card.Title>{name}</Card.Title>
        					<Card.Subtitle>Description</Card.Subtitle>
        					<Card.Text>{description}</Card.Text>
        					<Card.Subtitle>Price:</Card.Subtitle>
        					<Card.Text>P {price}.00</Card.Text>
        				<div style={{margin: '40px 20px'}}>
          			<Button variant="primary" className="mx-3"  as={Link} to={`/products/${_id}`}>Details</Button>
          			<Button variant="dark" className="mx-3" as={Link} to={"/order"}>Order</Button>
          				</div>
     				 </Card.Body>
   				 </Card>
		)
}

	