import '../App.css'
// import './FontAwesomeIcon'
import {useContext} from 'react';
import {Navbar, Nav, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function AppNavbar() {

  const {user} = useContext(UserContext);



  console.log(user)

	return (
<Navbar bg="dark" expand="lg" variant="dark">
      <Container>
        <Navbar.Brand as={Link} to ="/">CT Parcs</Navbar.Brand>
        <FontAwesomeIcon icon={['fab', 'laptop-mobile']} />
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse className="right-alinged" class="navbar">
          <Nav className="">
            <Nav.Link as={Link} to ="/products">Products</Nav.Link>
            <Nav.Link as={Link} to ="/cart">Cart</Nav.Link>
            <Nav.Link as={Link} to ="/order">Order</Nav.Link>
            {

            (user.id !== null) ?
                  <Nav.Link as={Link} to ="/logout">Logout</Nav.Link>
                  :
              <>  
              <Nav.Link as={Link} to ="/login">Login</Nav.Link>
              <Nav.Link as={Link} to ="/register">Register</Nav.Link>
              </>

            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>


		)
}