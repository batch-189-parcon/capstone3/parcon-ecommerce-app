import {Row, Col, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function Banner () {
	
	return(

			<Row>
				<Col className="p-5">
					<h1>Welcome to CT Parcs Merchandise!</h1>
					<p>Best quality at affordable price!</p>
					<Button variant="dark" as={Link} to={"/products"}>Click Here</Button>
				</Col>
			</Row>
		)
}