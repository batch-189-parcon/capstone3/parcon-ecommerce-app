import {useEffect, useState} from "react";
import {Row, Col, Card, Button} from 'react-bootstrap';
import '../App.css'
import { Link } from 'react-router-dom'
import Axios from "axios"

export default function Highlights ({courseProp}) {


	const [data, setData] = useState([]);

	useEffect(() => {
		Axios.get('https://vast-shelf-02215.herokuapp.com/products')
			 .then(response => {
			 	console.log(response.data);
			 	setData(response.data);
			 })
			 .catch(error => {
			 	console.error(error);
			 })
	},[])

	return (


		<Row className="mt-3 mb-3 pt-3 pd-3">

			{data.map((product, i) => {
				return (
					<Col xs={12} md={4} >
						<Card>
							<Card.Img variant="top" className="image" src={product.image} alt={product._id}/>
							<Card.Body>
								<Card.Title data-id={product._id} >{product.name}</Card.Title>
								<Card.Text>{product.description}</Card.Text>
								<Card.Text>P {product.price}.00</Card.Text>
								<Button variant="dark" as={Link} to={"/products"}>Buy Now!</Button>
							</Card.Body>
						</Card>
					</Col>
				)
			})}

{/*			<Col xs={12} md={4}>
				<Card style={{ width: '18rem' }}>
				      <Card.Img variant="top" class="image" src="https://gameone.ph/media/catalog/product/cache/7a2235b416a1900151232a782f707140/l/o/logitech-g502-hero-wireless-1.jpg"/>
				      <Card.Body>
				        <Card.Title>asd</Card.Title>
				        <Card.Text>asd</Card.Text>
				        <Button variant="primary" as={Link} to={"/products"}>Click Here</Button>
				      </Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4} pt={3}>
				<Card style={{ width: '18rem' }}>
					<Card.Img variant="top" class="image" src="https://gameone.ph/media/catalog/product/cache/7a2235b416a1900151232a782f707140/d/e/deathadder-v2-pro-wireless-1.jpg"/>
					<Card.Body>
				        <Card.Title>Razer DeathAdder Pro</Card.Title>
				        <Card.Text> Version 2 Wireless Gaming Mouse</Card.Text>
				        <Button variant="primary" as={Link} to={"/products"}>Click Here</Button>
					</Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card style={{ width: '18rem' }}>
					<Card.Img variant="top" src="https://onix.com.bd/public/uploads/all/gf5yLBiNUdXMzKgGrnyvWuOBgwoI5mVL5D3nklEB.png" />
					<Card.Body>
				        <Card.Title>Fantect X9</Card.Title>
				        <Card.Text> Marcro RGB Gaming Mouse</Card.Text>
						<Button variant="primary" as={Link} to={"/products"}>Click Here</Button>
					</Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card style={{ width: '18rem' }}>
					<Card.Img variant="top" src="https://www.tradeinn.com/f/13817/138171515/keep-out-hx901-7.1-gaming-headset-with-microphone.jpgg" />
					<Card.Body>
						<Card.Title>Keep Out HX901 7.1</Card.Title>
						<Card.Text>Gaming Headset with Microphone
						</Card.Text>
						<Button variant="primary" as={Link} to={"/products"}>Click Here</Button>
					</Card.Body>
				</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://www.tradeinn.com/f/13817/138171609/mars-gaming-mhax-gaming-headset-with-microphone.jpg" />
      <Card.Body>
        <Card.Title>Mars Gaming MHAX</Card.Title>
        <Card.Text>Gaming Headset With Microphone
        </Card.Text>
        <Button variant="primary" as={Link} to={"/products"}>Click Here</Button>
      </Card.Body>
    </Card>
				</Col>
				<Col xs={12} md={4}>
					<Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://shiftstore-eg.com/wp-content/uploads/2022/03/razer-kaira-x-playstation-2.jpg" />
      <Card.Body>
        <Card.Title>Razer Kaira X</Card.Title>
        <Card.Text>Wired Playstation Gaming Headset
        </Card.Text>
        <Button variant="primary" as={Link} to={"/products"}>Click Here</Button>
      </Card.Body>
    </Card>
				</Col>
				<Col xs={12} md={4}>
					<Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://gameone.ph/media/catalog/product/cache/7a2235b416a1900151232a782f707140/l/o/logitech-g502-hero-wireless-1.jpg" />
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
        <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
        <Button variant="primary" as={Link} to={"/products"}>Click Here</Button>
      </Card.Body>
    </Card>
				</Col>
				<Col xs={12} md={4}>
					<Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://gameone.ph/media/catalog/product/cache/7a2235b416a1900151232a782f707140/l/o/logitech-g502-hero-wireless-1.jpg" />
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
        <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
        <Button variant="primary" as={Link} to={"/products"}>Click Here</Button>
      </Card.Body>
    </Card>
				</Col>
				<Col xs={12} md={4} pt={3}>
					<Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://gameone.ph/media/catalog/product/cache/7a2235b416a1900151232a782f707140/l/o/logitech-g502-hero-wireless-1.jpg" />
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
        <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
        <Button variant="primary" as={Link} to={"/products"}>Click Here</Button>
      </Card.Body>
    </Card>

				</Col>*/}
		</Row>

    

		)



}