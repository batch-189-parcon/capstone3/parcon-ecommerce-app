// import {useState, useEffect, useContext} from 'react'
// import {Card, Button, Form} from 'react-bootstrap';
// import {Navigate} from 'react-router-dom'
// import Swal from 'sweetalert2';
// import UserContext from '../UserContext'

// export default function UpdateProduct() {

// 	const {user, setUser} = useContext(UserContext);


// 	const[name, setName] = useState("")
// 	const[description, setDescription] = useState("")
// 	const[price, setPrice] = useState(0)
// 	const[image, setImage] = useState("")
// 	const[isActive, setIsActive] = useState(false);





// 	function addProduct(event) {
// 		event.preventDefault()

// 		fetch(`https://vast-shelf-02215.herokuapp.com/products/:productId`, {
// 			method: "PUT",
// 			headers: {
// 				"Content-Type": "application/json",
// 				Authorization: `Bearer ${localStorage.getItem('token')}`
// 			},
// 			body: JSON.stringify({
// 				name: name,
// 				description: description,
// 				price: price,
// 				image: image
// 			})
// 		})

// 		.then(response => response.json())
// 		.then(data => {
// 			console.log(data);

// 			if (data === true) {
// 				Swal.fire({
// 					title: "Successfully Updated Product!",
// 					icon: "success",
// 					text: "Check status on product page."
// 				})

// 				Navigate("/products")

// 			} else {
// 				Swal.fire({
// 					title: "Something went wrong",
// 					icon: "error",
// 					text: "Please try again!" })
// 			}
// 		})

// 				setName("")
// 				setDescription("")
// 				setPrice("")
// 				setImage("")

// 	}

// 	const retrieveUserDetails = (token) => {

// 		fetch('https://vast-shelf-02215.herokuapp.com/users/details', {
// 				headers: {
// 						Authorization: `Bearer ${token}`
// 				}
// 		})
// 		.then(response => response.json())
// 		.then(data => {

// 			console.log(data)

// 			setUser({
// 				id: data._id,
// 				isAdmin: data.isAdmin
// 			})


// 		})


// 	} 



// 	useEffect(() => {

// 		if(name !== "" && description !=="" && price !== "") {
// 			setIsActive (true)
// 		} else {
// 			setIsActive (false)
// 		}


// 	}, [name, description, price])

// 	return (

// 	(name !== null) ?
// 		<Navigate to="/products"/>

// 		:

// 	 <div style={{padding: '40px 0px'}}>
// 		<Card 
// 			className="container d-flex flex-column justify-content-center align-items-center login-center Login col-md-8 col-lg-4 col-11 logcards"
// 			style={{
// 				padding: '24px 0px'
// 			}}

// 		>
// 		<Form className="mt-3" onSubmit={(event)=> addProduct(event)}>
// 	      <Form.Group className="mb-3" controlId="name">
// 	        <Form.Control 
// 	        type="name" 
// 	        placeholder="Name"
// 	        value = {name}
// 	        onChange = {event => {
// 	        	setName (event.target.value)
// 	        } }
// 	        required />
// 	      </Form.Group>

// 	      <Form.Group className="mb-3" controlId="description">
// 	        <Form.Control 
// 	        type="description" 
// 	        placeholder="Description"
// 	        value = {description}
// 	        onChange = {event => 
// 	        	setDescription (event.target.value)
// 	         }
// 	        required />
// 	       </Form.Group>

// 	       <Form.Group className="mb-3" controlId="price">
// 	        <Form.Control 
// 	        type="price" 
// 	        placeholder="Price"
// 	        value = {price}
// 	        onChange = {event => 
// 	        	setPrice (event.target.value)
// 	         }
// 	        required />
// 	       </Form.Group>

// 	       <Form.Group className="mb-3" controlId="image">
// 	        <Form.Control 
// 	        type="image" 
// 	        placeholder="Image URL"
// 	        value = {image}
// 	        onChange = {event => 
// 	        	setDescription (event.target.value)
// 	         }
// 	        required />
// 	       </Form.Group>
// 	      {
// 	      	//Ternary function
// 	      	isActive ?
// 	      		      <Button variant="primary" type="submit" id="submitBtn">
// 	        			Submit
// 	      				</Button>
// 	      			:
// 	      			<Button variant="secondary" type="submit" id="submitBtn" disabled>
// 	        			Submit
// 	      				</Button>
// 	      }

// 	    </Form>
// 	    </Card>
// 		</div>







// 		)






// }