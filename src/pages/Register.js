import { useState, useEffect, useContext } from 'react'
import {Form, Button, Card } from 'react-bootstrap'
import UserContext from '../UserContext'
import {Navigate, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import '../App.css'

export default function Register () {

	const {user} = useContext(UserContext);

	const navigate = useNavigate()

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	


 function registerUser(event) {
		event.preventDefault()

		fetch('https://vast-shelf-02215.herokuapp.com/users/checkEmail', {
			method: "POST",
			headers: {
					'Content-Type': 'application/json'
			},
				body: JSON.stringify({
						email: email
				})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
						title: "Duplicate Email Found",
						icon: "error",
						text: "Kindly provide another email to complete the registration."
				})
			} else {
					fetch('https://vast-shelf-02215.herokuapp.com/users/register', {
						method: "POST",
						headers: {
								'Content-Type': "application/json"
						},
						body: JSON.stringify({
							firstName:firstName,
							lastName: lastName,
							email: email,
							mobileNo: mobileNo,
							password: password1
						})
					})
					.then(response => response.json())
					.then(data => {

							console.log(data)
							if(data === true) {

								Swal.fire ({
										title: "Registration Success!",
										icon: "success",
										text: "You may now login"
								})

								setFirstName("");
								setLastName("");
								setEmail("");
								setMobileNo("");
								setPassword1("");
								setPassword2("");

								navigate("/login");

							} else {

									Swal.fire({
										title: "Something wrong!",
										icon: "Error",
										text: "Try again."
								})
							}
							

					})



			}

		
		})

		
		
	} 

	useEffect(() => {

		if((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !=="" && password2 !== "") && password1 === password2) {
			setIsActive (true)
		} else {
			setIsActive (false)
		}

		
	}, [firstName, lastName, email, mobileNo, password1, password2])


	return (

		(user.id !== null) ?
		<Navigate to="/products"/>
		:
		<div style={{padding: '40px 0px'}}>
			<Card className="text-center container d-flex flex-column justify-content-center align-items-center login-center Login col-md-8 col-lg-4 col-11">
			
			      <Card.Header>Welcome to Registration Page!</Card.Header>
			      <Card.Body>
			        <Form className="mt-3" onSubmit={(event)=> registerUser(event)}>

				<Form.Group className="mb-3" controlId="firstName">
			        <Form.Control 
			        type="text" 
			        placeholder="Enter First Name"
			        value = {firstName}
			        onChange = {event => {

			        		setFirstName(event.target.value)
			        	
			        }}
			        required />
			       
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="lastName">
			        <Form.Control 
			        type="text" 
			        placeholder="Enter Last Name"
			        value = {lastName}
			        onChange = {event => {
			        		setLastName(event.target.value)
			        				        }}
			        required />
			       
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Control 
			        type="email" 
			        placeholder="Enter email"
			        value = {email}
			        onChange = {event => {

			        		setEmail(event.target.value)
			        	
			        }}
			        required />			      
			        </Form.Group>

			      <Form.Group className="mb-3" controlId="mobileNo">
			        <Form.Control 
			        type="text" 
			        placeholder="Mobile Number"
			        value = {mobileNo}
			        onChange = {event => {
			        		setMobileNo(event.target.value)
			        	
			        }}
			        required />
			       
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password1">
			        <Form.Control 
			        type="password" 
			        placeholder="Password"
			        value = {password1}
			        onChange = {event => {
			        		setPassword1(event.target.value)
			        				         }}
			        required />
			       </Form.Group>

			      <Form.Group className="mb-3" controlId="password2">
			        <Form.Control 
			        type="password" 
			        placeholder="Password"
			        value = {password2}
			        onChange = {event => {
			        		setPassword2(event.target.value)
			        	}} 
			        required />
			      </Form.Group>
			      {
			      	//Ternary function
			      	isActive ?
			      		      <Button variant="primary" type="submit" id="submitBtn">
			        			Submit
			      				</Button>
			      			:
			      			<Button variant="danger" type="submit" id="submitBtn" disabled>
			        			Submit
			      				</Button>
			      }

			    </Form>
			      </Card.Body>
			      <Card.Footer className="text-muted">End of registration here</Card.Footer>
			    </Card>

		</div>
		)


}
