import { useState, useEffect, useContext } from 'react'
import {Form, Button, Card } from 'react-bootstrap'
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'
export default function Login () {

	
	const {user, setUser} = useContext(UserContext);

	
	const[email, setEmail] = useState("")
	const[password, setPassword] = useState("")
	const[isActive, setIsActive] = useState(false);


	function loginUser(event) {
		event.preventDefault()

		

		fetch('https://vast-shelf-02215.herokuapp.com/users/login', {
				method: 'POST',
				headers: {
						'Content-Type': 'application/json'
				},
				body: JSON.stringify({
						email: email,
						password: password
				})
		})
		.then(response => response.json())
		.then(data => {

				console.log(data)

				if(typeof data.access !== "undefined") {
					localStorage.setItem('token', data.access)
					retrieveUserDetails(data.access)

					Swal.fire({
						title: "Login Successful",
						icon: "success",
						text: "Welcome to CT Parcs Merchandise!"
					})

				} else {
						Swal.fire({
							title: "Authentication Failed",
							icon: "error",
							text: "Check your login details and try again!"
						})
				}
		})

		

		setEmail("")
		setPassword("")
	}

	const retrieveUserDetails = (token) => {

		fetch('https://vast-shelf-02215.herokuapp.com/users/details', {
				headers: {
						Authorization: `Bearer ${token}`
				}
		})
		.then(response => response.json())
		.then(data => {

			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})


		})


	} 

	useEffect(() => {

		if(email !== "" && password !=="") {
			setIsActive (true)
		} else {
			setIsActive (false)
		}

	}, [email, password])


	return (

	(user.id !== null) ?
		<Navigate to="/"/>

	: 

	<div style={{padding: '40px 0px'}}>
		<Card 
			className="container d-flex flex-column justify-content-center align-items-center login-center Login col-md-8 col-lg-4 col-11 logcards"
			style={{
				padding: '24px 0px'
			}}

		>
		<Form className="mt-3" onSubmit={(event)=> loginUser(event)}>
		<h1 className="text-center">Login</h1>
	      <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Control 
	        type="email" 
	        placeholder="Email"
	        value = {email}
	        onChange = {event => {
	        	setEmail (event.target.value)
	        } }
	        required />
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="password">
	        <Form.Control 
	        type="password" 
	        placeholder="Password"
	        value = {password}
	        onChange = {event => 
	        	setPassword (event.target.value)
	         }
	        required />
	       </Form.Group>
	      {
	      
	      	isActive ?
	      		      <Button variant="primary" type="submit" id="submitBtn">
	        			Submit
	      				</Button>
	      			:
	      			<Button variant="secondary" type="submit" id="submitBtn" disabled>
	        			Submit
	      				</Button>
	      }

	    </Form>
	    </Card>
	</div>





		)


}