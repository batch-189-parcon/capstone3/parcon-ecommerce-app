import { useContext, useEffect } from 'react'
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Logout () {

	
	const {unsetUser, setUser} = useContext(UserContext)

	
	unsetUser();

	
	useEffect(() => {
		
		setUser({id: null})

	}, [setUser])

	

	return (

			Swal.fire({
						title: "Logout Successful",
						icon: "success",
						text: "Visit us again!"
					}),

			<Navigate to="/" />


		)
}