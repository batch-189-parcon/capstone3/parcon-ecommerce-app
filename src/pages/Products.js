import {useContext, useEffect, useState} from 'react'
import UserView from "../components/UserView"
import AdminView from "../components/AdminView"
import UserContext from "../UserContext"



export default function Courses () {
	const { user } = useContext(UserContext);
	const [productsData, setProductsData] = useState([])
	const fetchData = () => {
		fetch(`https://vast-shelf-02215.herokuapp.com/products/`)
		.then(response => response.json())
		.then(data => {
	setProductsData(data)
		})
	}
	useEffect(() => {
		fetchData()
	}, [])

	return (
		(user.isAdmin) ?
		<AdminView coursesProp={productsData} fetchData={fetchData}/>
		:
		<UserView coursesProp={productsData}/>
		)

}












// import {Fragment, useEffect, useState} from 'react'
// import ProductCard from '../components/ProductCard';

// export default function Courses () {


// 	const [products, setProducts] = useState([])

// 	useEffect(() => {
// 		fetch('https://vast-shelf-02215.herokuapp.com/products/')
// 		.then(response => response.json())
// 		.then(data => {

// 			console.log(data)

// 			setProducts(data.map(product => {
// 				return (
// 						<ProductCard key={product._id} courseProp={product} />
// 					)
// 			}))
// 		})

// 	}, [])


// 	return (

		
// 		<Fragment>
// 			<h1>Products</h1>
// 			{products}
// 		</Fragment>




// 		)


// }