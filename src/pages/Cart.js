import {button} from 'react-bootstrap'


export default function Cart ({courseProp}){
// let { name, description, price, _id } = courseProp
	return(

		<div>
			<div className="py-3 bg-danger">
				<div className="Container text-center">
					<h3>Cart</h3>
				</div>
		</div>

		<div className="py-4">
			<div className="Container">
				<div className="row">
					<div className="col-md-12">

						<div className="table-responsive">
						<table className="table table-bordered">
							<thead>
								<tr>
									<th>Images</th>
									<th>Product</th>
									<th className="text-center">Price</th>
									<th className="text-center">Quantity</th>
									<th className="text-center">Total Price</th>
									<th>Order</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td width="10%">
										<img src="https://gameone.ph/media/catalog/product/cache/7a2235b416a1900151232a782f707140/d/e/deathadder-v2-pro-wireless-1.jpg" alt="Prod Image" width="50px" height="50px" />
									</td>
									<td>Razer DeathAdder Pro</td>
									<td width="15%" className="text-center">P 4599.00</td>
									<td width="15%">
										<div className="input-group">
											<button type="button" className="input-group-text">-</button>
											<div className="form-control text-center">1</div>
											<button type="button" className="input-group-text">+</button>
											
										</div>
									</td>
									<td width="15%" className="text-center">P 4599.00</td>
									<td width="10%">
										<button type="button" className="btn btn-dark btn-sm">Checkout</button>
										<button type="button" className="btn btn-danger btn-sm">Remove</button>
									</td>
								</tr>
							</tbody>
						</table>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>






		)
}