// import { Fragment} from 'react'
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Routes, Route} from 'react-router-dom'
import AppNavbar from './components/AppNavbar'
// import Banner from './components/Banner'
import NotFound from './components/NotFound'
// import Highlights from './components/Highlights'
import Cart from './pages/Cart'
import Home from './pages/Home'
import Products from './pages/Products'
import UpdateProduct from './pages/UpdateProduct'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Order from './pages/Orders'
import './App.css';
import { UserProvider } from './UserContext'

function App() {
  const [user, setUser] = useState({
    id:null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  
  useEffect(() => {
      fetch('http://localhost:4000/users/details', {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(response => response.json())
      .then(data => {
        if(typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          })

        }

      })

  }, [])

  return (
    //parent rule <></>
  <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
      <Container>
      <Routes>
       <Route path="/" element ={<Home/>}/>
       <Route path="/products" element ={<Products/>}/>
       <Route path="/products/:productid" element ={<UpdateProduct/>}/>
       <Route path="/cart" element ={<Cart/>}/>
       <Route path="/login" element ={<Login/>}/>
       <Route path="/logout" element ={<Logout/>}/>
       <Route path="/register" element ={<Register/>}/>
       <Route path="/order" element ={<Order/>}/>
       <Route path="*" element = {<NotFound/>}/>
      </Routes>
      </Container>
    </Router>
  </UserProvider>
  );
}

export default App;
